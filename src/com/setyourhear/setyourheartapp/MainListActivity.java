package com.setyourhear.setyourheartapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.setyourhear.setyourheartapp.R;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.PluginState;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainListActivity extends ListActivity {
	
	public static final int NUMBER_OF_POSTS = 20;
	public static final String TAG = MainListActivity.class.getSimpleName();
	protected JSONObject mBlogData;
	protected ProgressBar mProgressBar;
	protected ImageView logo;
	
	protected ImageView detailsButton;
	protected ImageView mediaButton;
	protected ImageView contactButton;
	
	private final String KEY_TITLE = "title";
	private final String KEY_AUTHOR = "excerpt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        logo = (ImageView) findViewById(R.id.logo);
        detailsButton = (ImageView) findViewById(R.id.imageView2);
        mediaButton = (ImageView) findViewById(R.id.imageView3);
        contactButton = (ImageView) findViewById(R.id.imageView4);
        
        detailsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//setContentView(R.layout.activity_blog_web_view);
        		
        		//Intent intent = getIntent();
            	//Intent intent = new Intent(this, this)

            	Intent intent = new Intent(getApplicationContext(), BlogWebViewActivity.class);
        		intent.setData(Uri.parse("http://philly14.com"));
        		startActivity(intent);
            	
            	
//        		Uri blogUri = intent.getData();
//        		
//        		WebView webView = (WebView) findViewById(R.id.webView1);
//        		
//        		WebSettings webSettings = webView.getSettings();
//        		webSettings.setJavaScriptEnabled(true); //Enables Javascript. Remove this line if your site doesn't require javascript
//        		webView.getSettings().setPluginState(PluginState.ON); //Enables plugins like Adobe flash. Remove if not required
//        		webView.loadUrl("http://philly14.com");
            }
        });
        
        mediaButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//setContentView(R.layout.activity_blog_web_view);
        		
        		//Intent intent = getIntent();
            	//Intent intent = new Intent(this, this)

            	Intent intent = new Intent(getApplicationContext(), BlogWebViewActivity.class);
        		intent.setData(Uri.parse("http://instagram.com/setyourheart14"));
        		startActivity(intent);
            	
            	
//        		Uri blogUri = intent.getData();
//        		
//        		WebView webView = (WebView) findViewById(R.id.webView1);
//        		
//        		WebSettings webSettings = webView.getSettings();
//        		webSettings.setJavaScriptEnabled(true); //Enables Javascript. Remove this line if your site doesn't require javascript
//        		webView.getSettings().setPluginState(PluginState.ON); //Enables plugins like Adobe flash. Remove if not required
//        		webView.loadUrl("http://philly14.com");
            }
        });
        
        contactButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//setContentView(R.layout.activity_blog_web_view);
        		
        		//Intent intent = getIntent();
            	//Intent intent = new Intent(this, this)

            	Intent intent = new Intent(getApplicationContext(), BlogWebViewActivity.class);
        		intent.setData(Uri.parse("http://robethany.org/philly/beta/contact-form/"));
        		startActivity(intent);
            	
            	
//        		Uri blogUri = intent.getData();
//        		
//        		WebView webView = (WebView) findViewById(R.id.webView1);
//        		
//        		WebSettings webSettings = webView.getSettings();
//        		webSettings.setJavaScriptEnabled(true); //Enables Javascript. Remove this line if your site doesn't require javascript
//        		webView.getSettings().setPluginState(PluginState.ON); //Enables plugins like Adobe flash. Remove if not required
//        		webView.loadUrl("http://philly14.com");
            }
        });
            
        if (isNetworkAvailable()) {
        	mProgressBar.setVisibility(View.VISIBLE);
        	logo.setVisibility(View.VISIBLE);
        	GetBlogPostsTask getBlogPostsTask = new GetBlogPostsTask();
        	getBlogPostsTask.execute();
        }
        else {
        	Toast.makeText(this, "Network is unavailable!", Toast.LENGTH_LONG).show();
        }
        
        //Toast.makeText(this, getString(R.string.no_items), Toast.LENGTH_LONG).show();
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	try {
    		JSONArray jsonPosts = mBlogData.getJSONArray("posts");
    		JSONObject jsonPost = jsonPosts.getJSONObject(position);
    		String blogUrl = jsonPost.getString("url");
    		
    		Intent intent = new Intent(this, BlogWebViewActivity.class);
    		intent.setData(Uri.parse(blogUrl));
    		startActivity(intent);
    	}
    	catch (JSONException e) {
    		logException(e);
    	}
    }
    
    protected void onClickDetails(View v){
    	
    }
    
    

    private void logException(Exception e) {
    	Log.e(TAG, "Exception caught!", e);
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager manager = (ConnectivityManager) 
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = manager.getActiveNetworkInfo();
		
		boolean isAvailable = false;
		if (networkInfo != null && networkInfo.isConnected()) {
			isAvailable = true;
		}
		
		return isAvailable;
	}

	public void handleBlogResponse() {
		mProgressBar.setVisibility(View.INVISIBLE);
		logo.setVisibility(View.INVISIBLE);
		
		if (mBlogData == null) {
			updateDisplayForError();
		}
		else {
			try {
				JSONArray jsonPosts = mBlogData.getJSONArray("posts");
				ArrayList<HashMap<String, String>> blogPosts = 
						new ArrayList<HashMap<String, String>>();
				for (int i = 0; i < jsonPosts.length(); i++) {
					JSONObject post = jsonPosts.getJSONObject(i);
					String title = post.getString(KEY_TITLE);
					title = Html.fromHtml(title).toString();
					String author = post.getString(KEY_AUTHOR);
					author = Html.fromHtml(author).toString();
					
					HashMap<String, String> blogPost = new HashMap<String, String>();
					blogPost.put(KEY_TITLE, title);
					blogPost.put(KEY_AUTHOR, author);
					
					blogPosts.add(blogPost);
				}
				
				String[] keys = { KEY_TITLE, KEY_AUTHOR };
				int[] ids = { android.R.id.text1, android.R.id.text2 };
				SimpleAdapter adapter = new SimpleAdapter(this, blogPosts,
						android.R.layout.simple_list_item_2, 
						keys, ids);
				
				setListAdapter(adapter);
			} 
			catch (JSONException e) {
				logException(e);
			}
		}
	}

	private void updateDisplayForError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.error_title));
		builder.setMessage(getString(R.string.error_message));
		builder.setPositiveButton(android.R.string.ok, null);
		AlertDialog dialog = builder.create();
		dialog.show();
		
		TextView emptyTextView = (TextView) getListView().getEmptyView();
		emptyTextView.setText(getString(R.string.no_items));
	}
    
    private class GetBlogPostsTask extends AsyncTask<Object, Void, JSONObject> {

		@Override
		protected JSONObject doInBackground(Object... arg0) {
			int responseCode = -1;
			JSONObject jsonResponse = null;
			
	        try {
	        	URL blogFeedUrl = new URL("http://philly14.com/?json=1");
	        	HttpURLConnection connection = (HttpURLConnection) blogFeedUrl.openConnection();
	        	connection.connect();
	        	
	        	responseCode = connection.getResponseCode();
	        	if (responseCode == HttpURLConnection.HTTP_OK) {
	        		InputStream inputStream = connection.getInputStream();
	        		//Reader reader = new InputStreamReader(inputStream);
	        		BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
//	        		int contentLength = connection.getContentLength();
//	        		char[] charArray = new char[contentLength];
//	        		reader.read(charArray);
	        		StringBuilder sb = new StringBuilder();
	        	    int cp;
	        	    while ((cp = rd.read()) != -1) {
	        	      sb.append((char) cp);
	        	    }
	        		String responseData = new String(sb.toString());
	        		
	        		jsonResponse = new JSONObject(responseData);
	        	}
	        	else {
	        		Log.i(TAG, "Unsuccessful HTTP Response Code: " + responseCode);
	        	}
	        }
	        catch (MalformedURLException e) {
	        	logException(e);
	        }
	        catch (IOException e) {
	        	logException(e);
	        }
	        catch (Exception e) {
	        	logException(e);
	        }
	        
	        return jsonResponse;
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			mBlogData = result;
			handleBlogResponse();
		}
    	
    }
    
}
